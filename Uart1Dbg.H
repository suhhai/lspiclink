
#ifndef __UART1_DEBUG_H__
#define __UART1_DEBUG_H__
#include "stdio.h"
extern void   UART1Init( );
extern UINT8  CH554UART1RcvByte( );
extern void   CH554UART1SendByte(UINT8 SendDat);
#endif
#endif